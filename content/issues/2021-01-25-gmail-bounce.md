---
title: Email delivery problems to Google
date: 2021-01-25 21:40:00
resolved: true
resolvedWhen: 2021-01-25 21:40:00
# Possible severity levels: down, disrupted, notice
severity: notice
affected:
  - Email
  - Mailing lists
section: issue
---

Google mail servers are currently bouncing some emails coming from
`@torproject.org`, as sent from our main mail server (`eugeni`) and
third-party servers. This includes lists but may not include all our
mail servers (the donation platform, for example, seems to still
work).

We are still investigating the problem, followup in [issue 40149](https://gitlab.torproject.org/tpo/tpa/team/-/issues/40149).

**Update, 22:16UTC**: it looks like only *some* emails are being
bounced, especially a particular email from a particular sysadmin
which made him jump the gun and post this disruption notice. We might
actually be in our normal "somewhat disrupted" delivery
situation. [Stay tuned for more happy days][].

[Stay tuned for more happy days]: https://en.wikipedia.org/wiki/Buddy_Holly_(song)

**Update, 2021-01-27 16:51UTC**: it turns out this was a false alarm,
and concerns only a single Google group that is refusing
emails. Emails are being delivered to Google fine.
